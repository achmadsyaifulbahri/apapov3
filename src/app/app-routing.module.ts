import { DukcapilPage } from './informasi/dukcapil/dukcapil.page';
import { P2lokasikantorPage } from './permohonan/p2lokasikantor/p2lokasikantor.page';
import { LandingpagePage } from './landingpage/landingpage.page';
import { LoginPage } from './login/login.page';
import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { RegistrasiPage } from './registrasi/registrasi.page';
import { PanduanPage } from './panduan/panduan.page';
import { InformasiPage } from './informasi/informasi.page';
import { PermohonanPage } from './permohonan/permohonan.page';
import { P1jenispasporPage } from './permohonan/p1jenispaspor/p1jenispaspor.page';
import { P3listpermohonanPage } from './permohonan/p3listpermohonan/p3listpermohonan.page';
import { P4cekdukcapilPage } from './permohonan/p4cekdukcapil/p4cekdukcapil.page';
import { P5getdukcakpilPage } from './permohonan/p5getdukcakpil/p5getdukcakpil.page';
import { P7datakeluargaPage } from './permohonan/p7datakeluarga/p7datakeluarga.page';
import { P8datapasanganPage } from './permohonan/p8datapasangan/p8datapasangan.page';
import { P9tanggalPage } from './permohonan/p9tanggal/p9tanggal.page';
import { P10jamPage } from './permohonan/p10jam/p10jam.page';
import { P11statusPage } from './permohonan/p11status/p11status.page';
import { P12statusdetailPage } from './permohonan/p12statusdetail/p12statusdetail.page';
import { P6datadiriPage } from './permohonan/p6datadiri/p6datadiri.page';
import { P13dataverifikasiPage } from './permohonan/p13dataverifikasi/p13dataverifikasi.page';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'landingpage',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: './home/home.module#HomePageModule'
  },
  {
    path: 'list',
    loadChildren: './list/list.module#ListPageModule'
  },
  {
    path: 'login',
    component: LoginPage
  },
  {
    path: 'registrasi',
    component: RegistrasiPage
  },
  {
    path: 'landingpage',
    component: LandingpagePage
  },
  {
    path: 'panduan',
    component: PanduanPage
  },
  {
    path: 'informasi',
    component: InformasiPage
  },
  {
    path: 'permohonan',
    component: PermohonanPage
  },
  {
    path: 'p1jenispaspor',
    component: P1jenispasporPage
  },
  {
    path: 'p2lokasikantor',
    component: P2lokasikantorPage
  },
  {
    path: 'p3listpermohonan',
    component: P3listpermohonanPage
  },
  {
    path: 'p4cekdukcapil',
    component: P4cekdukcapilPage
  },
  {
    path: 'p5getdukcakpil',
    component: P5getdukcakpilPage
  },
  {
    path: 'p6datadiri',
    component: P6datadiriPage
  },
  {
    path: 'p7datakeluarga',
    component: P7datakeluargaPage
  },
  {
    path: 'p8datapasangan',
    component: P8datapasanganPage
  },
  {
    path: 'p9tanggal',
    component: P9tanggalPage
  },
  {
    path: 'p10jam',
    component: P10jamPage
  },
  {
    path: 'p11status',
    component: P11statusPage
  },
  {
    path: 'p12statusdetail',
    component: P12statusdetailPage
  },
  {
    path: 'p13dataverifikasi',
    component: P13dataverifikasiPage
  },
  {
    path: 'dukcapil',
    component: DukcapilPage
  },
  { path: 'profile', loadChildren: './profile/profile.module#ProfilePageModule' },
  { path: 'p0tujuan', loadChildren: './permohonan/p0tujuan/p0tujuan.module#P0tujuanPageModule' },
  { path: 'go-pendaftaran', loadChildren: './modals/alert/go-pendaftaran/go-pendaftaran.module#GoPendaftaranPageModule' },
  { path: 'lupapassword', loadChildren: './lupapassword/lupapassword.module#LupapasswordPageModule' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
