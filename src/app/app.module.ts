import { GoPendaftaranPage } from './modals/alert/go-pendaftaran/go-pendaftaran.page';
import { IonicSelectableModule } from 'ionic-selectable';
import { LoginPage } from './login/login.page';
import { RegistrasiPage } from './registrasi/registrasi.page';
import { LandingpagePage } from './landingpage/landingpage.page';
import { PermohonanPage } from './permohonan/permohonan.page';
import { P1jenispasporPage } from './permohonan/p1jenispaspor/p1jenispaspor.page';
import { P2lokasikantorPage } from './permohonan/p2lokasikantor/p2lokasikantor.page';
import { P3listpermohonanPage } from './permohonan/p3listpermohonan/p3listpermohonan.page';
import { P4cekdukcapilPage } from './permohonan/p4cekdukcapil/p4cekdukcapil.page';
import { P5getdukcakpilPage } from './permohonan/p5getdukcakpil/p5getdukcakpil.page';
import { P6datadiriPage } from './permohonan/p6datadiri/p6datadiri.page';
import { P7datakeluargaPage } from './permohonan/p7datakeluarga/p7datakeluarga.page';
import { P8datapasanganPage } from './permohonan/p8datapasangan/p8datapasangan.page';
import { P9tanggalPage } from './permohonan/p9tanggal/p9tanggal.page';
import { P10jamPage } from './permohonan/p10jam/p10jam.page';
import { P11statusPage } from './permohonan/p11status/p11status.page';
import { P12statusdetailPage } from './permohonan/p12statusdetail/p12statusdetail.page';
import { P13dataverifikasiPage } from './permohonan/p13dataverifikasi/p13dataverifikasi.page';
import { PanduanPage } from './panduan/panduan.page';
import { InformasiPage } from './informasi/informasi.page';
import { DukcapilPage } from './informasi/dukcapil/dukcapil.page'
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { DatePickerModule } from 'ionic4-date-picker';
import { DukcapilPageModule } from './informasi/dukcapil/dukcapil.module';
import { DukcapilService } from './services/dukcapil.service';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { HTTP } from '@ionic-native/http/ngx'
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { registerLocaleData } from '@angular/common';
import localeid from '@angular/common/locales/id';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { EsriLoaderService } from 'angular-esri-loader';

registerLocaleData(localeid, 'id');
@NgModule({
  declarations:
  [AppComponent,
   LoginPage,
   RegistrasiPage,
   LandingpagePage,
   PermohonanPage,
   P1jenispasporPage,
   P2lokasikantorPage,
   P3listpermohonanPage,
   P4cekdukcapilPage,
   P5getdukcakpilPage,
   P6datadiriPage,
   P7datakeluargaPage,
   P8datapasanganPage,
   P9tanggalPage,
   P10jamPage,
   P11statusPage,
   P12statusdetailPage,
   P13dataverifikasiPage,
   PanduanPage,
   InformasiPage,
   DukcapilPage,
   GoPendaftaranPage
  ],
  entryComponents: [
    LoginPage,
    RegistrasiPage,
    LandingpagePage,
    PermohonanPage,
    P1jenispasporPage,
    P2lokasikantorPage,
    P3listpermohonanPage,
    P4cekdukcapilPage,
    P5getdukcakpilPage,
    P6datadiriPage,
    P7datakeluargaPage,
    P8datapasanganPage,
    P9tanggalPage,
    P10jamPage,
    P11statusPage,
    P12statusdetailPage,
    PanduanPage,
    InformasiPage,
    P13dataverifikasiPage,
    DukcapilPage,
    GoPendaftaranPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    // DukcapilPageModule,
    DatePickerModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    }),
    BrowserAnimationsModule,
    IonicSelectableModule
    // NgbModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    DukcapilService,
    HTTP,
    Geolocation,
    EsriLoaderService,
    { provide:
      RouteReuseStrategy,
      useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
