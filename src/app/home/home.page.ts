import { Component } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { DukcapilService } from '../services/dukcapil.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss']
})
export class HomePage {
  constructor(
    private alertController: AlertController,
    private router: Router,
    private gss: DukcapilService
  ) {}

  async pasporrusak() {
    const alert = await this.alertController.create({
      header: 'Pemberitahuan',
      message:
        'Untuk paspor rusak atau hilang, silakan laporkan ke kantor imigrasi terdekat'
    });

    await alert.present();
  }

  async pasporbaru() {
    const alert = await this.alertController.create({
      header: 'Pemberitahuan',
      message:
        'Untuk membuat paspor baru, ada beberapa hal yang harus dipersiapkan, yaitu :<br>1. Kartu Tanda Penduduk<br>2. Kartu Keluarga<br>3. Akte Kelahiran<br>4. Ijazah Terakhir',
        buttons: [
          {
            text: 'Belum Siap',
            role: 'cancel',
            cssClass: 'secondary',
            handler: (blah) => {
              // console.log('Confirm Cancel: blah');
            }
          }, {
            text: 'Siap !!',
            handler: () => {
              console.log('Confirm Okay');
              this.gss.inidlayanan = '43092ce2-613d-4ac6-b86f-898070a97d33';
              this.gss.innamalayanan = 'Baru';
              this.router.navigate(['/p0tujuan']);
            }
          }
        ]
    });
    await alert.present();
  }
  async gantipaspor() {
    const alert = await this.alertController.create({
      header: 'Pemberitahuan',
      message:
      'Untuk penggantian/perpanjang paspor, ada beberapa hal yang harus dipersiapkan, yaitu :<br>1. Kartu Tanda Penduduk<br>2. Kartu Keluarga<br>3. Akte Kelahiran<br>4. Ijazah Terakhir',
        buttons: [
          {
            text: 'Belum Siap',
            role: 'cancel',
            cssClass: 'secondary',
            handler: (blah) => {
              // console.log('Confirm Cancel: blah');
            }
          }, {
            text: 'Siap !!',
            handler: () => {
              console.log('Confirm Okay');
              this.gss.inidlayanan = 'ba06c4bf-5aee-4ac7-a793-a9fd8fc576ed';
              this.gss.innamalayanan = 'Ganti';
              this.router.navigate(['/p0tujuan']);
            }
          }
        ]
    });
    await alert.present();
  }
}
