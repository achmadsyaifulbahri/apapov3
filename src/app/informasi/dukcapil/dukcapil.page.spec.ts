import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DukcapilPage } from './dukcapil.page';

describe('DukcapilPage', () => {
  let component: DukcapilPage;
  let fixture: ComponentFixture<DukcapilPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DukcapilPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DukcapilPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
