import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';

@Component({
  selector: 'app-dukcapil',
  templateUrl: './dukcapil.page.html',
  styleUrls: ['./dukcapil.page.scss'],
})
export class DukcapilPage implements OnInit {
  constructor(private navParams: NavParams, private modalController: ModalController) { }

  ngOnInit() {
  }
  closeModal() {
    this.modalController.dismiss();
  }

}
