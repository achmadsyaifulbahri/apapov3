import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InformasiPage } from './informasi.page';

describe('InformasiPage', () => {
  let component: InformasiPage;
  let fixture: ComponentFixture<InformasiPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InformasiPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InformasiPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
