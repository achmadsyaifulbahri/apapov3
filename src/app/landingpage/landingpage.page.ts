import { GoPendaftaranPage } from './../modals/alert/go-pendaftaran/go-pendaftaran.page';
import { ModalController, PopoverController, AlertController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-landingpage',
  templateUrl: './landingpage.page.html',
  styleUrls: ['./landingpage.page.scss'],
})
export class LandingpagePage implements OnInit {

  constructor(
    public modalController: ModalController,
    public popoverController: PopoverController,
    public alertController: AlertController
    ) { }

  ngOnInit() {
    this.openModal();
    // tslint:disable-next-line: no-unused-expression
    // this.popoverController();
  }

  async openModal(){
    const modal = await this.modalController.create({
      component: GoPendaftaranPage
    });
    modal.present();
  }

  async alerts() {
    const alert = await this.alertController.create({
      header: 'Pemberitahuan',
      message: 'Email anda belum terdaftar, Daftar sekarang !',
      buttons: [
        {
          text: 'Sudah Terdaftar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Daftar !!',
          handler: () => {
            console.log('Confirm Okay');
          }
        }
      ]
    });

    await alert.present();
  }
}
