import { DukcapilService } from 'src/app/services/dukcapil.service';
import { LoginuserService } from './../services/loginuser.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { AlertController, LoadingController } from '@ionic/angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { of } from 'rxjs';
import { catchError, finalize } from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  public temp = {};
  public isloading = null;
  public tempuser = '';
  public temppass = '';
  public user = '';
  public pass = '';
  
  private loginMsg = '';

  constructor(
    private alertController: AlertController, 
    private router: Router,
    private lus: LoginuserService,
    private gss: DukcapilService,
    private loadingCtrl: LoadingController,
    private http: HttpClient
    ) { }

  ngOnInit() {

  }

  //nedd to be fixed
  async submitlogin() {
    console.log(this.tempuser , '+' , this.temppass)
    let loading = await this.loadingCtrl.create({
      message: 'Mohon Tunggu'
    });
    await loading.present();
    this.http
			.post(
				'http://10.0.11.33:5001/layanan/wni/user/login',
				{ body: { USERNAME: this.tempuser, PASSWORD: this.temppass } },
				{
					headers: {
						'Content-Type': 'application/json',
						// ; charset=utf-8',
						// 'Accept': 'application/json',
						'Access-control-allow-origin': '*',
						// 'Access-Control-Allow-Methods': 'GET,POST',
						// 'Access-Control-Allow-Headers': 'X-Requested-With'
					}
				}
			)
			.pipe(finalize(() => loading.dismiss()))
			.toPromise()
			.then((res: any) => {
        console.log(res);
        this.loginMsg = res.MESSAGE;
        if (res.CODE ==='200'){
          this.router.navigate(['/home']);
        }else{
          this.loginGagalAlert();
        }
			});
		// subscribe(
		// 	(data) => {
		// 		this.data = data['results'];
		// 	},
		// 	(err) => {
		// 		console.log('JS Call error: ', err);
		// 	}
    // );
    
    // this.lus
    //   .getdata(this.tempuser, this.temppass)
    //   .then(async (data: any) => {
    //     console.log('ini data dari gss', data);
    //     this.temp = data;
    //     this.loginMsg = data.MESSAGE;
    //     loading.dismiss();
    //     if (data.MESSAGE = 'LOGIN SUCCESS'){
    //       this.router.navigate(['/home']);
    //     }else{
    //       this.loginGagalAlert();
    //     }
    //   })
    //   .catch(err => {
    //     // this.errorLoginMsg = err.
    //     loading.dismiss();
    //     alert(JSON.stringify(err));
        
    //   });
    // // this.router.navigate(['/home']);
  }

  logingoogle() {
    alert( 'sudah punya akun google?' );
  }
  async alert1() {
    const alert = await this.alertController.create({
      header: 'Pemberitahuan',
      // subHeader: 'Subtitle',
      message: 'Email anda belum terdaftar, Daftar sekarang ?',
      buttons: [{
        text: 'Daftar !!',
        handler: () => {
          console.log('Confirm Okay');
          this.router.navigate(['/registrasi']);
        }
      }]
    });
    await alert.present();
  }

  async alert2() {
    const alert = await this.alertController.create({
      header: 'Pemberitahuan',
      message: 'Email anda belum terdaftar, Daftar sekarang !',
      buttons: [
        {
          text: 'Sudah Terdaftar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          } 
        }, {
          text: 'Daftar !!',
          handler: () => {
            console.log('Confirm Okay');
            this.router.navigate(['/registrasi']);
          }
        }
      ]
    });

    await alert.present();
  }

  async loginGagalAlert() {
    const alert = await this.alertController.create({
      header: 'Login Gagal',
      message: this.loginMsg,
      buttons: ['OK']
    });

    await alert.present();
  }
}
