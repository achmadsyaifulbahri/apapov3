import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { GoPendaftaranPage } from './go-pendaftaran.page';

const routes: Routes = [
  {
    path: '',
    component: GoPendaftaranPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [GoPendaftaranPage]
})
export class GoPendaftaranPageModule {}
