import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GoPendaftaranPage } from './go-pendaftaran.page';

describe('GoPendaftaranPage', () => {
  let component: GoPendaftaranPage;
  let fixture: ComponentFixture<GoPendaftaranPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoPendaftaranPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GoPendaftaranPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
