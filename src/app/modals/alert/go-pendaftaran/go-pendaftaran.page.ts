import { ModalController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-go-pendaftaran',
  templateUrl: './go-pendaftaran.page.html',
  styleUrls: ['./go-pendaftaran.page.scss'],
})
export class GoPendaftaranPage implements OnInit {

  constructor(public modalController: ModalController, public router: Router) { }

  ngOnInit() {
  }
  lanjut() {
    this.modalController.dismiss();
    this.router.navigate(['/login']);
  }
}
