import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { P0tujuanPage } from './p0tujuan.page';

const routes: Routes = [
  {
    path: '',
    component: P0tujuanPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [P0tujuanPage]
})
export class P0tujuanPageModule {}
