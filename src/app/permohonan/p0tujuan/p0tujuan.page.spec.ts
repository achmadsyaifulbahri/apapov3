import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { P0tujuanPage } from './p0tujuan.page';

describe('P0tujuanPage', () => {
  let component: P0tujuanPage;
  let fixture: ComponentFixture<P0tujuanPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ P0tujuanPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(P0tujuanPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
