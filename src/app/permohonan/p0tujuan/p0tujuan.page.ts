import { DukcapilService } from 'src/app/services/dukcapil.service';
import { DatamasterService } from './../../services/datamaster.service';
import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-p0tujuan',
  templateUrl: './p0tujuan.page.html',
  styleUrls: ['./p0tujuan.page.scss']
})
export class P0tujuanPage implements OnInit {
  public mastertujuan;
  public tujuanterpilih;
  constructor(
    private alertController: AlertController,
    private router: Router,
    private dm: DatamasterService,
    private gss: DukcapilService
  ) {}
  ngOnInit() {
    this.dm.getLocalDataTujuan().subscribe(tujuan => {
      this.mastertujuan = this.dm.dtujuan;
    });
  }

  async pasporbaru() {
    this.router.navigate(['/p1jenispaspor']);
  }
  tujuans() {
    if ( this.tujuanterpilih != null) {
      this.gss.inidtujuan = this.tujuanterpilih;
      console.log('ada tujuan', this.mastertujuan);
      console.log('adaaaa tujuan', this.tujuanterpilih);
      this.router.navigate(['/p1jenispaspor']);
    } else {
      console.log('kosong');
    }
  }
  tujuanHaji() {
    this.gss.inidtujuan = this.dm.dtujuan[0].id;
    this.gss.innamatujuan = this.dm.dtujuan[0].desk_tujuan_dok_perjalanan;
    console.log('id', this.gss.inidtujuan);
    console.log('nama tujuan', this.gss.innamatujuan);
    this.router.navigate(['/p1jenispaspor']);
    console.log('data semua', this.gss);
  }
  tujuanUmroh() {
    this.gss.inidtujuan = this.dm.dtujuan[1].id;
    this.gss.innamatujuan = this.dm.dtujuan[1].desk_tujuan_dok_perjalanan;
    console.log('id', this.gss.inidtujuan);
    console.log('nama tujuan', this.gss.innamatujuan);
    this.router.navigate(['/p1jenispaspor']);

  }
    tujuanBekerjaFormal() {
      this.gss.inidtujuan = this.dm.dtujuan[2].id;
      this.gss.innamatujuan = this.dm.dtujuan[2].desk_tujuan_dok_perjalanan;
      console.log('id', this.gss.inidtujuan);
      console.log('nama tujuan', this.gss.innamatujuan);
      this.router.navigate(['/p1jenispaspor']);

  }
    tujuanWisata() {
      this.gss.inidtujuan = this.dm.dtujuan[3].id;
      this.gss.innamatujuan = this.dm.dtujuan[3].desk_tujuan_dok_perjalanan;
      console.log('id', this.gss.inidtujuan);
      console.log('nama tujuan', this.gss.innamatujuan);
      this.router.navigate(['/p1jenispaspor']);

  }
  tujuanBelajar() {
    this.gss.inidtujuan = this.dm.dtujuan[4].id;
    this.gss.innamatujuan = this.dm.dtujuan[4].desk_tujuan_dok_perjalanan;
    console.log('id', this.gss.inidtujuan);
    console.log('nama tujuan', this.gss.innamatujuan);
    this.router.navigate(['/p1jenispaspor']);

  }
    tujuanTKI() {
      this.gss.inidtujuan = this.dm.dtujuan[5].id;
      this.gss.innamatujuan = this.dm.dtujuan[5].desk_tujuan_dok_perjalanan;
      console.log('id', this.gss.inidtujuan);
      console.log('nama tujuan', this.gss.innamatujuan);
      this.router.navigate(['/p1jenispaspor']);

  }


}
