import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { P10jamPage } from './p10jam.page';

describe('P10jamPage', () => {
  let component: P10jamPage;
  let fixture: ComponentFixture<P10jamPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ P10jamPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(P10jamPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
