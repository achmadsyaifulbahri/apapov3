import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { P12statusdetailPage } from './p12statusdetail.page';

const routes: Routes = [
  {
    path: '',
    component: P12statusdetailPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [P12statusdetailPage]
})
export class P12statusdetailPageModule {}
