import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { P12statusdetailPage } from './p12statusdetail.page';

describe('P12statusdetailPage', () => {
  let component: P12statusdetailPage;
  let fixture: ComponentFixture<P12statusdetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ P12statusdetailPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(P12statusdetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
