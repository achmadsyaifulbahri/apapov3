import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { P13dataverifikasiPage } from './p13dataverifikasi.page';

const routes: Routes = [
  {
    path: '',
    component: P13dataverifikasiPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [P13dataverifikasiPage]
})
export class P13dataverifikasiPageModule {}
