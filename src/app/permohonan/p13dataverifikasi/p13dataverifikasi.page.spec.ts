import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { P13dataverifikasiPage } from './p13dataverifikasi.page';

describe('P13dataverifikasiPage', () => {
  let component: P13dataverifikasiPage;
  let fixture: ComponentFixture<P13dataverifikasiPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ P13dataverifikasiPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(P13dataverifikasiPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
