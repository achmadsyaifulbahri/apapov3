import { DatamasterService } from 'src/app/services/datamaster.service';
import { Router } from '@angular/router';
import { DukcapilService } from 'src/app/services/dukcapil.service';
import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-p13dataverifikasi',
  templateUrl: './p13dataverifikasi.page.html',
  styleUrls: ['./p13dataverifikasi.page.scss'],
})
export class P13dataverifikasiPage  implements OnInit {
  constructor(public gss: DukcapilService, private router: Router, public dm: DatamasterService) { }
  content = document.getElementById('listIon');
  hideMe: boolean;
  public namatujuan: any;
  
  ngOnInit() {
    console.log('data semua', this.gss);
    
    this.hideMe = false;
  }
  daftar() {
    console.log('siap post', this.gss);
    this.router.navigate(['/p2lokasikantor']);
  }
}
