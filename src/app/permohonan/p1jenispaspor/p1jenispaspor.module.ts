import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { P1jenispasporPage } from './p1jenispaspor.page';

const routes: Routes = [
  {
    path: '',
    component: P1jenispasporPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [P1jenispasporPage]
})
export class P1jenispasporPageModule {}
