import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { P1jenispasporPage } from './p1jenispaspor.page';

describe('P1jenispasporPage', () => {
  let component: P1jenispasporPage;
  let fixture: ComponentFixture<P1jenispasporPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ P1jenispasporPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(P1jenispasporPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
