import { DatamasterService } from './../../services/datamaster.service';
import { DukcapilService } from './../../services/dukcapil.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-p1jenispaspor',
  templateUrl: './p1jenispaspor.page.html',
  styleUrls: ['./p1jenispaspor.page.scss'],
})
export class P1jenispasporPage implements OnInit {

  constructor(
    private gss: DukcapilService,
    private dm: DatamasterService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  jenisnonel() {
    // this.gss.inidjenis = this.dm.djenis[1].id;
    // this.gss.innamajenis = this.dm.djenis[1].desk_jenis_dok_perjalanan;
    this.gss.inidjenis = '4d39004f-0a19-4019-bc5f-f82c6b2e1630';
    this.gss.innamajenis = 'Paspor Non-Elektronik';
    this.gss.inhargajenis = 'Rp. 350.000,00-';
    this.router.navigate(['/p4cekdukcapil']);
  }
  jenisel() {
    this.gss.inidjenis = 'f1bde1e0-a366-4657-979c-309c1c9e98eb';
    this.gss.innamajenis = 'Paspor Elektronik';
    this.gss.inhargajenis = 'Rp. 650.000,00-';
    this.router.navigate(['/p4cekdukcapil']);
  }

}
