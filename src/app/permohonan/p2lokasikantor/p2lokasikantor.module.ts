import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { P2lokasikantorPage } from './p2lokasikantor.page';

const routes: Routes = [
  {
    path: '',
    component: P2lokasikantorPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [P2lokasikantorPage]
})
export class P2lokasikantorPageModule {}
