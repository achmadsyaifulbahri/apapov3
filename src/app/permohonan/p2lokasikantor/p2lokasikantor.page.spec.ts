import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { P2lokasikantorPage } from './p2lokasikantor.page';

describe('P2lokasikantorPage', () => {
  let component: P2lokasikantorPage;
  let fixture: ComponentFixture<P2lokasikantorPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ P2lokasikantorPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(P2lokasikantorPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
