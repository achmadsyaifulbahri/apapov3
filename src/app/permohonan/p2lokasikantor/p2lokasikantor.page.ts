import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { DatamasterService } from 'src/app/services/datamaster.service';
import { Platform, AlertController } from '@ionic/angular';
import { Router, NavigationExtras } from '@angular/router';
import { loadModules } from 'esri-loader';
// import { SimpleFillSymbol, SimpleMarkerSymbol } from 'esri/symbols';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { EsriLoaderService } from 'angular-esri-loader';

@Component({
	selector: 'app-p2lokasikantor',
	templateUrl: './p2lokasikantor.page.html',
	styleUrls: [ './p2lokasikantor.page.scss' ]
})
export class P2lokasikantorPage implements OnInit {
	// @ViewChild('map', { static: true }) mapEl: ElementRef;
	@ViewChild('map') mapEl: ElementRef;
	mapView: any = null;
	public masterkanim: any;
	public filteredMasterKanim: any;
	public datakanim: any;
	public pilihkanim = '';
	private curLat: any;
	private curLong: any;
	public kanimID: any = [];

	public selectedKanimId = '';

	map: any;
	polylineJson: any = {
		paths: [ [] ],
		spatialReference: { wkid: 102100 }
	};
	kanimLat: any = [];
	kanimLong: any = [];
	points: any = [
		// [ this.dm.dkanim.items.latitude,  this.dm.dkanim.items.longitude ]
		// [ 12708134.9029992, 3580175.4635029174 ],
		// [ 12707700.167400816, 3579950.9297323236 ],
		// [ 12707012.234146232, 3579864.9380755005 ],
		// [ 12706854.58277539, 3579812.3876185534 ],
		// [ 12706668.26751894, 3579611.7404192993 ],
		// [ 12706563.166605044, 3579415.8705343134 ],
		// [ 12706243.086549092, 3579520.971448208 ],
		// [ 12706362.519405792, 3579898.3792753764 ],
		// [ 12706610.939747725, 3580036.9213891467 ],
		// [ 12707461.30168742, 3580251.9005312044 ],
		// [ 12707590.289172653, 3580294.896359616 ],
		// [ 12708240.003913095, 3580667.526872516 ],
		// [ 12708794.172368176, 3581188.2541277227 ],
		// [ 12709424.777851546, 3581551.330012087 ],
		// [ 12709553.76533678, 3581269.4684702777 ]
	];

	listKanim: Array<any>;

	constructor(
		private dm: DatamasterService,
		public platform: Platform,
		public alertCtrl: AlertController,
		private geoloc: Geolocation,
		private esriLoader: EsriLoaderService,
		private router: Router
	) {}

	ngOnInit() {
		// console.log(this.dm.dkanim.items.latitude, ' , ' , this.dm.dkanim.items.longitude)
		this.getCurLoc();
		// this.loadMap();

		this.dm
			.getdatakanim(
				'5CD9317A-317A-355A-1EBE-E0531400A8C0',
				'',
				'1',
				'10',
				'0',
				'1',
				'8F71A37BDE1C6555E053A202010AD897'
			)
			.then((data: any) => {
				console.log('ini data dari detail kanim', data);
			});

		this.dm.getLocalDatakanim().subscribe((kanim) => {
			let kanims = this.dm.dkanim.items;
			for (let kanim of kanims) {
				this.masterkanim = this.dm.dkanim.items;
				this.filteredMasterKanim = this.masterkanim;
				// console.log(this.masterkanim);
			}
		});
	}

	kanimterpilih(id) {
		// console.log('kanimID', id);
		this.dm.getAvailableQuota(id, '9071FC06950BF5A3E053A202010AF981').then((data: any) => {
			// console.log('quota available', data);
			if (data.AVAILABLE == undefined) {
				// console.log('Kuota tidak tersedia');
				this.alerthabis();
			} else {
				// console.log('suskses');
				data.AVAILABLE.map((item: any, index: any) => {
					
				});
				let navigationExtras: NavigationExtras = {
					queryParams: {
						data: JSON.stringify(data.AVAILABLE)
					}
				};
				// console.log('extras',navigationExtras);
				this.router.navigate(['p9tanggal'], navigationExtras);
			}
		});
	}

	getItems(event) {
		this.filteredMasterKanim = this.masterkanim;
		const val = event.target.value;
		if (val && val.trim() !== '') {
			this.filteredMasterKanim = this.masterkanim.filter((item) => {
				return item.unit_name.toLowerCase().indexOf(val.toLowerCase()) > -1;
				// return item.unit_name.toLowerCase().includes(val.toLowerCase());
			});
		}
		// } else {
		// 	return this.masterkanim;
		// }
	}

	async getGeo() {
		// Reference: https://ionicframework.com/docs/api/platform/Platform/#ready
		await this.platform.ready();

		// Load the mapping API modules

		const [ Map, MapView, Compass, Graphic, Point, PictureMarkerSymbol ]: any = await loadModules([
			'esri/Map',
			'esri/views/MapView',
			'esri/widgets/Compass',
			'esri/Graphic',
			'esri/geometry/Point',
			'esri/symbols/PictureMarkerSymbol'
		]).catch((err) => {
			console.error('ArcGIS: ', err);
		});

		console.log('Starting up ArcGIS map');

		let map = new Map({
			basemap: 'osm'
		});

		let mapView = new MapView({
			// create the map view at the DOM element in this component
			container: this.mapEl.nativeElement,
			center: [ this.curLong, this.curLat ],
			zoom: 8,
			map: map
		});

		let compassWidget = new Compass({
			view: mapView
		});

		let picSymbol = new PictureMarkerSymbol({
			url: 'https://www.easyicon.net/api/resizeApi.php?id=1185658&size=32',
			height: 40,
			width: 40
		});

		let point = new Point({
			// longitude: this.curLong,
			// latitude: this.curLat
			longitude: [ this.kanimLat ],
			latitude: [ this.kanimLong ]
		});

		let pointgraphic = new Graphic({
			geometry: point,
			symbols: picSymbol
		});

		// Add the Compass widget to the top left corner of the view
		mapView.ui.add(compassWidget, 'top-left');
		mapView.graphics.add(pointgraphic);
	}

	// loadMap() {
	// 	// only load the ArcGIS API for JavaScript when this component is loaded
	// 	return this.esriLoader
	// 		.load({
	// 			url: 'https://js.arcgis.com/4.8/'
	// 			// url: 'http://192.168.0.222/arcgis_js_api/library/3.17/3.17/init.js' // 如果没有搭建gis函数则需使用在线的
	// 		})
	// 		.then(() => {
	// 			// 模块载入要一一对应
	// 			this.esriLoader
	// 				.loadModules([
	// 					'esri',
	// 					'esri/map',
	// 					'esri/layers/ArcGISTiledMapServiceLayer',
	// 					'esri/toolbars/draw',
	// 					'esri/symbols/SimpleMarkerSymbol',
	// 					'esri/symbols/SimpleLineSymbol',
	// 					'esri/symbols/PictureMarkerSymbol',
	// 					'esri/geometry/Point',
	// 					'esri/geometry/Polyline',
	// 					'esri/geometry/Extent',
	// 					'esri/layers/GraphicsLayer',
	// 					'esri/symbols/PictureFillSymbol',
	// 					'esri/symbols/CartographicLineSymbol',
	// 					'esri/SpatialReference',
	// 					'esri/InfoTemplate',
	// 					'esri/graphic',
	// 					// 'dojo',
	// 					// 'dojo/_base/Color',
	// 					// 'dojo/dom',
	// 					// 'dojo/on',
	// 					// 'dojo/domReady!'
	// 				])
	// 				.then(
	// 					(
	// 						[
	// 							esri,
	// 							Map,
	// 							ArcGISTiledMapServiceLayer,
	// 							Draw,
	// 							SimpleMarkerSymbol,
	// 							SimpleLineSymbol,
	// 							PictureMarkerSymbol,
	// 							Point,
	// 							Polyline,
	// 							Extent,
	// 							GraphicsLayer,
	// 							PictureFillSymbol,
	// 							CartographicLineSymbol,
	// 							SpatialReference,
	// 							InfoTemplate,
	// 							Graphic,
	// 							dojo,
	// 							Color,
	// 							dom,
	// 							on
	// 						]
	// 					) => {
	// 						// create the map at the DOM element in this component
	// 						this.map = new Map(this.mapEl.nativeElement, {
	// 							center: [ this.curLat, this.curLong ], // 地图初始中心点
	// 							zoom: 15, // 缩放比例
	// 							logo: false // 隐藏logo
	// 							// slider:false // 隐藏侧边缩放工具
	// 							// basemap: 'osm' // 不用基础图层
	// 						});
	// 						// 载入地图服务
	// 						const myTiledMapServiceLayer = new ArcGISTiledMapServiceLayer(
	// 							'http://cache1.arcgisonline.cn/arcgis/rest/services/ChinaOnlineCommunity_Mobile/MapServer'
	// 							// 'http://server.arcgisonline.com/ArcGIS/rest/services/NGS_Topo_US_2D/MapServer'
	// 						);
	// 						this.map.addLayer(myTiledMapServiceLayer);

	// 						// dojo载入事件添加
	// 						dojo.connect(this.map, 'onLoad', (evt) => {
	// 							// 点标记
	// 							const newPoint = new Point(
	// 								12709568.097279584,
	// 								3581250.359213206,
	// 								new SpatialReference({ wkid: 102100 })
	// 							);
	// 							const picSymbol = new PictureMarkerSymbol(
	// 								'http://www.easyicon.net/api/resizeApi.php?id=1185658&size=32',
	// 								25,
	// 								32
	// 							);
	// 							const graphic = new Graphic(newPoint, picSymbol);
	// 							this.map.graphics.add(graphic);

	// 							// 加入测试点数据
	// 							for (let i = 0; i < this.points.length; i++) {
	// 								this.polylineJson.paths[0].push([ this.points[i][0], this.points[i][1] ]);
	// 							}
	// 							const polyline = new Polyline(this.polylineJson);
	// 							// 线样式DASH虚线SOLID实线
	// 							const sys = new SimpleLineSymbol(
	// 								SimpleLineSymbol.STYLE_DASH,
	// 								new esri.Color([ 16, 142, 233 ]),
	// 								3
	// 							);
	// 							const graphic2 = new Graphic(polyline, sys);
	// 							this.map.graphics.add(graphic2); // 添加曲线
	// 						});

	// 						// dojo点击事件添加
	// 						dojo.connect(this.map, 'onClick', (evt) => {
	// 							const emp = evt.mapPoint;
	// 							const cur_wkid = emp.spatialReference.wkid; // 当前坐标系
	// 							console.log('当前地图的坐标系为:wkid==>' + cur_wkid);
	// 							console.log('刚才点击的墨卡托为:x==>' + emp.x + '  y==>' + emp.y);
	// 							// 点击添加一个点
	// 							const newPoint = new Point(emp.x, emp.y, new SpatialReference({ wkid: 102100 }));
	// 							const picSymbol = new PictureMarkerSymbol(
	// 								'http://www.easyicon.net/api/resizeApi.php?id=1185658&size=32',
	// 								25,
	// 								32
	// 							);
	// 							const graphic = new Graphic(newPoint, picSymbol);
	// 							this.map.graphics.add(graphic);
	// 						});
	// 					}
	// 				);
	// 		});
	// }

	async getCurLoc() {
		this.geoloc
			.getCurrentPosition()
			.then((resp) => {
				this.curLat = resp.coords.latitude;
				this.curLong = resp.coords.longitude;
				this.getGeo();
			})
			.catch((error) => {
				console.log('Error getting location', error);
			});
	}

	// async getKanimLoc() {
	// 	// Reference: https://ionicframework.com/docs/api/platform/Platform/#ready
	// 	await this.platform.ready();

	// 	// Load the mapping API modules

	// 	const [
	// 		Map,
	// 		MapView,
	// 		Compass,
	// 		Graphic,
	// 		Point,
	// 		SimpleMarkerSymbol,
	// 		PictureMarkerSymbol
	// 	]: any = await loadModules([
	// 		'esri/Map',
	// 		'esri/views/MapView',
	// 		'esri/widgets/Compass',
	// 		'esri/Graphic',
	// 		'esri/geometry/Point',
	// 		'esri/symbols/SimpleMarkerSymbol',
	// 		'esri/symbols/PictureMarkerSymbol'
	// 	]).catch((err) => {
	// 		console.error('ArcGIS: ', err);
	// 	});

	// 	let kanimPoints = new Point({
	// 		latitude: this.masterkanim.latitude[0],
	// 		longitude: this.masterkanim.longitude[0]
	// 	});
	// }

	// selectKanim() {
	// 	console.log(this.kanimterpilih);
	// }

	async alerthabis() {
		const alert = await this.alertCtrl.create({
			header: 'Warning',
			message: 'Maaf kuota tidak tersedia di Kantor Pelayanan ini'
		});
		await alert.present();
	}
}
