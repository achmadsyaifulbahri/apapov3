import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { P3listpermohonanPage } from './p3listpermohonan.page';

const routes: Routes = [
  {
    path: '',
    component: P3listpermohonanPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [P3listpermohonanPage]
})
export class P3listpermohonanPageModule {}
