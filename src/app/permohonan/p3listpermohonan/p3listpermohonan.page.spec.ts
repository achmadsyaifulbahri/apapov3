import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { P3listpermohonanPage } from './p3listpermohonan.page';

describe('P3listpermohonanPage', () => {
  let component: P3listpermohonanPage;
  let fixture: ComponentFixture<P3listpermohonanPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ P3listpermohonanPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(P3listpermohonanPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
