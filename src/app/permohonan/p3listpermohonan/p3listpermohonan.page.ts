import { DukcapilService } from 'src/app/services/dukcapil.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-p3listpermohonan',
  templateUrl: './p3listpermohonan.page.html',
  styleUrls: ['./p3listpermohonan.page.scss'],
})
export class P3listpermohonanPage implements OnInit {

  constructor(public gss: DukcapilService) { }

  ngOnInit() {
    console.log('list', this.gss);
  }

}
