import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { P4cekdukcapilPage } from './p4cekdukcapil.page';

const routes: Routes = [
  {
    path: '',
    component: P4cekdukcapilPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [P4cekdukcapilPage]
})
export class P4cekdukcapilPageModule {}
