import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { P4cekdukcapilPage } from './p4cekdukcapil.page';

describe('P4cekdukcapilPage', () => {
  let component: P4cekdukcapilPage;
  let fixture: ComponentFixture<P4cekdukcapilPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ P4cekdukcapilPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(P4cekdukcapilPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
