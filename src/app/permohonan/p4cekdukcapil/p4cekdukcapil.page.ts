import { AlertController, ModalController, LoadingController } from '@ionic/angular';
// import { alertController } from '@ionic/core';
import { DukcapilService } from './../../services/dukcapil.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { DukcapilPage } from 'src/app/informasi/dukcapil/dukcapil.page';

@Component({
  selector: 'app-p4cekdukcapil',
  templateUrl: './p4cekdukcapil.page.html',
  styleUrls: ['./p4cekdukcapil.page.scss']
})
export class P4cekdukcapilPage implements OnInit {
  public temp = {};
  public isloading = null;
  public tempinnik = '';
  public tempinibu = '';
  public nik = '';
  public namalengkap = '';
  public ibu = '';
  value = null;

  constructor(
    private httpclient: HttpClient,
    private router: Router,
    private gss: DukcapilService,
    private alertController: AlertController,
    private modalController: ModalController,
    private loadingCtrl: LoadingController
  ) {}

  ngOnInit() {}

  async alertnamaibusalah() {
    const alert = await this.alertController.create({
      header: 'Nama Ibu Tidak Sesuai',
      // subHeader: 'Subtitle',
      message: 'Mohon input Nama ibu sesuai dengan kartu keluarga'
      // buttons: [
      //   {
      //     text: 'Info Dukcapil',
      //     handler: () => {
      //       console.log('Confirm Okay');
      //       this.router.navigate(['/dukcapil']);
      //     }
      // }]
    });
    alert.present();
  }
  async alertnik() {
    const alert = await this.alertController.create({
      header: 'NIK tidak Terdaftar di Dukcapil',
      // subHeader: 'Subtitle',
      message: 'Mohon input NIK yang benar'
      // buttons: [
      //   {
      //     text: 'Info Dukcapil',
      //     handler: () => {
      //       console.log('Confirm Okay');
      //       this.router.navigate(['/dukcapil']);
      //     }
      // }]
    });
    alert.present();
  }
  async alertniknull() {
    const alert = await this.alertController.create({
      header: 'Pemberitahuan',
      // subHeader: 'Subtitle',
      message: 'NIK tidak boleh kosong'
      // buttons: [
      //   {
      //     text: 'Info Dukcapil',
      //     handler: () => {
      //       console.log('Confirm Okay');
      //       this.router.navigate(['/dukcapil']);
      //     }
      // }]
    });
    alert.present();
  }
  async submitdukcapil() {
    // this.gss.intempnik = '6171026001960006';
    // this.gss.getagama = 'ISLAM';
    // this.gss.getalamat = 'GG. MABANG 2 NO. 20';
    // this.gss.getkabid = '71';
    // this.gss.getkecid = '2';
    // this.gss.getkelid = '1004';
    // this.gss.getpropid = '61';
    // this.gss.getkerja = 'PELAJAR/MAHASISWA';
    // this.gss.getkabnama = 'KOTA PONTIANAK';
    // this.gss.getkecnama = 'PONTIANAK TIMUR';
    // this.gss.getkelnama = 'TANJUNGHULU';
    // this.gss.getnamalgkp = 'MENTARI MAHARDINA';
    // this.gss.getnamalgkpibu = 'PARINA';
    // this.gss.getpropnama = 'KALIMANTAN BARAT';
    // this.gss.getnik = '6171026001960006';
    // this.gss.getnokk = '6171023107070015';
    // this.gss.getpddk = 'BELUM TAMAT SD/SEDERAJAT';
    // this.gss.getrt = '1';
    // this.gss.getrw = '7';
    // this.gss.getstatus = 'BELUM KAWIN';
    // this.gss.gettgllhr = '1996-01-20';
    // this.gss.gettmptlhr = 'PONTIANAK';
    let loading = await this.loadingCtrl.create({
      message: 'Mohon Tunggu'
    });
    await loading.present();
    this.gss
      .getdata('012317501imigras', this.tempinnik, 'WKaUWhiR')
      .then(async (data: any) => {
        loading.dismiss();
        console.log('ini data dari gss', data);
        this.temp = data;
        this.isloading = false;
        this.nik = data.content[0].NIK;
        this.namalengkap = data.content[0].NAMA_LGKP;
        this.ibu = data.content[0].NAMA_LGKP_IBU;
        console.log(this.nik);
        console.log(this.ibu);
        if (this.tempinnik === this.nik) {
          if ((this.tempinibu.toLocaleUpperCase()) === this.ibu) {
            this.router.navigate(['/p5getdukcakpil']);
            this.gss.intempnik = this.tempinnik;
            this.gss.getnik = this.tempinnik;
            this.gss.getnokk = data.content[0].NO_KK;
            this.gss.getnamalgkp = data.content[0].NAMA_LGKP;
            this.gss.getnamalgkpibu = data.content[0].NAMA_LGKP_IBU;
            this.gss.getagama = data.content[0].AGAMA;
            this.gss.gettgllhr = data.content[0].TGL_LHR;
            this.gss.gettmptlhr = data.content[0].TMPT_LHR;
            this.gss.getstatus = data.content[0].STATUS_KAWIN;
            this.gss.getpddk =  data.content[0].PDDK_AKH;
            this.gss.getkerja = data.content[0].JENIS_PKRJN;
            this.gss.getalamat = data.content[0].ALAMAT;
            this.gss.getrt =  data.content[0].NO_RT;
            this.gss.getrw =  data.content[0].NO_RW;
            this.gss.getkabid = data.content[0].NO_KAB;
            this.gss.getkecid = data.content[0].NO_KEC;
            this.gss.getkelid = data.content[0].NO_KEL;
            this.gss.getpropid = data.content[0].NO_PROP;
            this.gss.getkabnama = data.content[0].KAB_NAME;
            this.gss.getkecnama = data.content[0].KEC_NAME;
            this.gss.getkelnama = data.content[0].KEL_NAME;
            this.gss.getpropnama = data.content[0].PROP_NAME;
            console.log('get', this);
          } else {
            this.gss.getnamalgkpibu = data.content[0].NAMA_LGKP_IBU;
            console.log('nama ibu ', this.gss.getnamalgkpibu)
            this.alertnamaibusalah();
          }
        } else {
          this.alertnik();
        }
      })
      .catch(err => {
        alert(JSON.stringify(err));
      });
  }
  // async openModal() {
  //   const modal = await this.modalController.create({
  //     component: DukcapilPage
  //   });
  //   modal.present();
  // }
}
