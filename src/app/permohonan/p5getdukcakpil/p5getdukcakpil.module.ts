import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { P5getdukcakpilPage } from './p5getdukcakpil.page';

const routes: Routes = [
  {
    path: '',
    component: P5getdukcakpilPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [P5getdukcakpilPage]
})
export class P5getdukcakpilPageModule {}
