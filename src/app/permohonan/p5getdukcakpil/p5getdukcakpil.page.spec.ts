import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { P5getdukcakpilPage } from './p5getdukcakpil.page';

describe('P5getdukcakpilPage', () => {
  let component: P5getdukcakpilPage;
  let fixture: ComponentFixture<P5getdukcakpilPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ P5getdukcakpilPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(P5getdukcakpilPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
