import { DukcapilService } from './../../services/dukcapil.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-p5getdukcakpil',
  templateUrl: './p5getdukcakpil.page.html',
  styleUrls: ['./p5getdukcakpil.page.scss'],
})
export class P5getdukcakpilPage implements OnInit {

  public NIK = '';
  public NO_KK = '';
  public NAMA_LGKP = '';
  public NAMA_LGKP_IBU = '';
  public TMPT_LHR = '';
  public TGL_LHR = '';
  public AGAMA = '';
  public STATUS_KAWIN = '';
  public PDDK_AKH = '';
  public JENIS_PKRJN = '';
  public ALAMAT = '';
  public NO_RT = '';
  public NO_RW = '';
  public KEL_NAME = '';
  public KEC_NAME = '';
  public KAB_NAME = '';
  public PROP_NAME = '';
  public KEL_NO = '';
  public KEC_NO = '';
  public KAB_NO = '';
  public PROP_NO = '';
  public tempinnik = '';
  public tempinibu = '';

  constructor(private gss: DukcapilService, private router: Router) {}

  ngOnInit() {
    // this.gss
    // .getdata('012317501imigras', this.gss.intempnik, 'WKaUWhiR')
    //   .then((data: any) => {
    //     console.log('ini data dari detail', data);
        // this.NIK = (this.gss.getnik = data.content[0].NIK);
        // this.NO_KK = (this.gss.getnokk = data.content[0].NO_KK);
        // this.NAMA_LGKP = (this.gss.getnamalgkp = data.content[0].NAMA_LGKP);
        // this.NAMA_LGKP_IBU = (this.gss.getnamalgkpibu = data.content[0].NAMA_LGKP_IBU);
        // this.TMPT_LHR = (this.gss.gettmplahir = data.content[0].TMPT_LHR);
        // this.TGL_LHR = (this.gss.gettgllahir = data.content[0].TGL_LHR);
        // this.AGAMA = (this.gss.getagama = data.content[0].AGAMA);
        // this.STATUS_KAWIN = (this.gss.getstatus = data.content[0].STATUS_KAWIN);
        // this.PDDK_AKH = (this.gss.getpddk = data.content[0].PDDK_AKH);
        // this.JENIS_PKRJN = (this.gss.getkerja = data.content[0].JENIS_PKRJN);
        // this.ALAMAT = (this.gss.getalamat = data.content[0].ALAMAT);
        // this.NO_RT = (this.gss.getrt = data.content[0].NO_RT);
        // this.NO_RW = (this.gss.getrw = data.content[0].NO_RW);
        // this.KEL_NAME = (this.gss.getnamakel = data.content[0].KEL_NAME);
        // this.KEC_NAME = (this.gss.getnamakec = data.content[0].KEC_NAME);
        // this.KAB_NAME = (this.gss.getnamakab = data.content[0].KAB_NAME);
        // this.PROP_NAME = (this.gss.getnamaprop = data.content[0].PROP_NAME);
        // this.KEL_NO = (this.gss.getidkel = data.content[0].NO_KEL);
        // this.KEC_NO = (this.gss.getidkec = data.content[0].NO_KEC);
        // this.KAB_NO = (this.gss.getidkab = data.content[0].NO_KAB);
        // this.PROP_NO = (this.gss.getidprop = data.content[0].NO_PROP);
        // this.gss.getagama = 'ISLAM';
        // this.gss.getalamat = 'GG. MABANG 2 NO. 20';
        // this.gss.getidkab = '71';
        // this.gss.getidkec = '2';
        // this.gss.getidkel = '1004';
        // this.gss.getidprop = '61';
        // this.gss.getkerja = 'PELAJAR/MAHASISWA';
        // this.gss.getnamakab = 'KOTA PONTIANAK';
        // this.gss.getnamakec = 'PONTIANAK TIMUR';
        // this.gss.getnamakel = 'TANJUNGHULU';
        // this.gss.getnamalgkp = 'MENTARI MAHARDINA';
        // this.gss.getnamalgkpibu = 'PARINA';
        // this.gss.getnamaprop = 'KALIMANTAN BARAT';
        // this.gss.getnik = '6171026001960006';
        // this.gss.getnokk = '6171023107070015';
        // this.gss.getpddk = 'BELUM TAMAT SD/SEDERAJAT';
        // this.gss.getrt = '1';
        // this.gss.getrw = '7';
        // this.gss.getstatus = 'BELUM KAWIN';
        // this.gss.gettgllahir = '1996-01-20';
        // this.gss.gettmplahir = 'PONTIANAK';
      // });
  }
  lanjut() {
    this.router.navigate(['/p6datadiri']);
  }
}
