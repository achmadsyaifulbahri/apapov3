export interface DataTujuan {
    "id": string;
    "is_active": number;
    "desk_tujuan_dok_perjalanan": string;
    "id_tujuan_dok_perjalanan": number;
    "id_user": string;
    "curr_version": number;
    "sync_version": number;
}

export interface DataKodepos {
    "id": string;
    "is_active": number;
    "id_kode_pos": string;
    "id_kecamatan": string;
    "id_user": string;
    "curr_version": number;
    "sync_version": number;
}

export interface DataKecamatan {
    "id_uuid": string;
    "is_active":number;
    "id_kecamatan": string;
    "nama": string;
    "id_kabupaten": string;
    "id_user": string
}
export interface DataKabupaten {
    "id_uuid": string;
    "is_active": number,
    "id_kabupaten": string;
    "name": string;
    "id_provinsi": string;
    "id_user": string;
}

export interface DataProvinsi {
    "id_uuid": string;
    "is_active": number;
    "id_kabupaten": string;
    "name": string;
    "id_provinsi": string;
    "id_user": string;

}

