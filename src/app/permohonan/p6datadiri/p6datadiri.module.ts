import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { P6datadiriPage } from './p6datadiri.page';

const routes: Routes = [
  {
    path: '',
    component: P6datadiriPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [P6datadiriPage]
})
export class P6datadiriPageModule {}
