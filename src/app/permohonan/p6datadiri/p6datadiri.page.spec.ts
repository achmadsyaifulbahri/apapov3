import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { P6datadiriPage } from './p6datadiri.page';

describe('P6datadiriPage', () => {
  let component: P6datadiriPage;
  let fixture: ComponentFixture<P6datadiriPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ P6datadiriPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(P6datadiriPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
