import { IonicSelectableComponent } from 'ionic-selectable';
import { DataTujuan } from './p6datadiri.model';
import { DatamasterService } from './../../services/datamaster.service';
import { DukcapilService } from './../../services/dukcapil.service';
import { Router } from '@angular/router';
import { Component, OnInit, Renderer2, ɵConsole } from '@angular/core';

@Component({
  selector: 'app-p6datadiri',
  templateUrl: './p6datadiri.page.html',
  styleUrls: ['./p6datadiri.page.scss']
})
export class P6datadiriPage implements OnInit {
  datatujuans: DataTujuan[];
  content = document.getElementById('listIon');
  hideMe: boolean;
  masterkabupaten: any;
  mastercity: any;
  masterkecamatan: any;
  masterdistric: any;
  masterkodepos: any;

  public NOHP = '';
  public email = '';
  public nokerabat = '';
  public namakerabat = '';
  public jeniskelamin = '';
  public idpekerjaan = '';
  public namapekerjaan = '';
  public domisilialamat = '';
  public domisiliidkel = '';
  public domisiliidkec = '';
  public domisiliidkab = '';
  public domisiliidprop = '';
  public domisilinamakel = '';
  public domisilinamakec = '';
  public domisilinamakab = '';
  public domisilinamaprop = '';
  
  public domalamat = '';
  public domidkel = '';
  public domidkec = '';
  public domidkab = '';
  public domidprop = '';
  public domnamakel = '';
  public domnamakec = '';
  public domnamakab = '';
  public domnamaprop = '';
  public getdomisilialamat = '';
  public getdomisilirt = '';
  public getdomisilirw = '';
  public getdomisilikel = '';
  public getdomisilikec = '';
  public getdomisilikab = '';
  public getdomisiliprop = '';

  public idtujuan = '';
  public namatujuan = '';
  public tujuannama1 = '';
  public mastertujuan: any;
  public masterpekerjaan: any;
  public masterkodeposs: any;
  public masterkecamatans: any;
  public masterkabupatens: any;
  public masterprovinsi: any;
  public masteradmin_area: any;
  public mastercitys: any;
  public masterdistrics: any;

  constructor(
    private gss: DukcapilService,
    private router: Router,
    private dm: DatamasterService
  ) {}

  ngOnInit() {
    // Data Dukcapil
    this.hideMe = false;
    // this.getdomisilialamat = this.gss.getalamat;
    // this.getdomisilirt = this.gss.getrt;
    // this.getdomisilirw = this.gss.getrw;
    // this.getdomisilikel = this.gss.getkel;
    // this.getdomisilikec = this.gss.getkec;
    // this.getdomisilikab = this.gss.getkab;
    // this.getdomisiliprop = this.gss.getprop;
    // ==================================================================================
    // ==============================
    // Data Master Tujuan
    this.dm.getLocalDataTujuan().subscribe(tujuans => {
      this.mastertujuan = this.dm.dtujuan;
      // console.log('data master tujuan', this.mastertujuan);
    });
    // ==============================
    // ==============================
    // Data Master Tujuan
    this.dm.getLocalDataPekerjaan().subscribe(pekerjaans => {
      this.masterpekerjaan = this.dm.dpekerjaan;
      // console.log('data master tujuan', this.masterpekerjaan);
    });
    // ==============================
    // ==============================
    // Data Master Provinsi
    this.dm.getLocalDataProvinsi().subscribe(provinsis => {
      this.masterprovinsi = this.dm.dprovinsi;
      // console.log('data master provinsi', this.masterprovinsi);
    });
        // Data Master admin_area
        this.dm.getLocalDataadmin_area().subscribe(admin_areas => {
          this.masteradmin_area = this.dm.dadmin_area;
          // console.log('data master admin_area', this.masteradmin_area);
        });
    // Data Master kabupaten
    this.dm.getLocalDataKabupaten().subscribe(kabupatens => {
      this.masterkabupatens = this.dm.dkabupaten;
      // console.log('service', this.masterkabupaten);
    });
        // Data Master kabupaten
        this.dm.getLocalDatacity().subscribe(citys => {
          this.mastercitys = this.dm.dcity;
          // console.log('service', this.mastercity);
        });
    // Data Master kecamtan
    this.dm.getLocalDataKecamatan().subscribe(kecamatan => {
      this.masterkecamatans = this.dm.dkecamatan;
      // console.log('service', this.masterkabupaten);
    });
    this.dm.getLocalDataKecamatan().subscribe(kecamatan => {
      this.masterkecamatans = this.dm.dkecamatan;
      // console.log('service', this.masterkabupaten);
    });
    // Data Master kodepos
    this.dm.getLocalDataKodePos().subscribe(kodepos => {
      this.masterkodeposs = this.dm.dkodepos;
      // console.log('service', this.masterkabupaten);
    });
  }

  pekerjaans(event: { component: IonicSelectableComponent; value: any }) {
    const pilihpekerjaan = event.value;
    this.idpekerjaan = pilihpekerjaan.id_pekerjaan;
    this.namapekerjaan = pilihpekerjaan.nama_pekerjaan;
    // console.log('idprovinsi', this.idpekerjaan, this.namapekerjaan);
  }
  tujuans(event: { component: IonicSelectableComponent; value: any }) {
    const pilihtujuan = event.value;
    this.idtujuan = pilihtujuan.id_tujuan_dok_perjalanan;
    this.namatujuan = pilihtujuan.desk_tujuan_dok_perjalanan;
    // console.log('idprovinsi', this.idtujuan, this.namatujuan);
  }

  filterkabupatens(event: { component: IonicSelectableComponent; value: any }) {
    const pilihprovinsi = event.value;
    console.log('provinsi', pilihprovinsi);
    const idprovinsi = pilihprovinsi.id_provinsi;
    this.domisiliidprop = pilihprovinsi.id_provinsi;
    this.domisilinamaprop = pilihprovinsi.nama_provinsi;

    this.domidprop = this.domisiliidprop;
    this.domnamaprop = this.domisilinamaprop;
    console.log('idprovinsi', this.domisiliidprop, this.domisilinamaprop);
    // master kabupaten yang sudah di filter
    console.log('service kabupaten', this.masterkabupatens);
    const filkabupatens = this.masterkabupatens.filter(
      kabupaten => kabupaten.id_provinsi == idprovinsi
    );
    this.masterkabupaten = filkabupatens;
    console.log('kabupaten', this.masterkabupaten);
  }
  filterkecamatans(event: { component: IonicSelectableComponent; value: any }) {
    const pilihkabupaten = event.value;
    console.log('kabupaten', pilihkabupaten); 
    const idkabupaten = pilihkabupaten.id_kabupaten;
    this.domisiliidkab = pilihkabupaten.id_kabupaten;
    this.domisilinamakab = pilihkabupaten.name;
    this.domidkab = this.domisiliidkab;
    this.domnamakab = this.domisilinamakab;
    console.log('kabupaten', this.domisiliidkab, this.domisilinamakab);
    // master kabupaten yang sudah di filter
    console.log('service kecamatan', this.masterkecamatans);
    const filkecamatans = this.masterkecamatans.filter(
      kecamatan => kecamatan.id_kabupaten == idkabupaten
    );
    this.masterkecamatan = filkecamatans;
    console.log('kecamataan', this.masterkecamatan); 
  }

  // filtercitys(event: { component: IonicSelectableComponent; value: any }) {
  //   const pilihprovinsi = event.value;
  //   console.log('provinsi', pilihprovinsi);
  //   const idprovinsi = pilihprovinsi.id;
  //   this.domisiliidprop = pilihprovinsi.id;
  //   this.domisilinamaprop = pilihprovinsi.area_name;
  //   console.log('idprovinsi', this.domisiliidprop, this.domisilinamaprop);
  //   // master city yang sudah di filter
  //   console.log('service city', this.mastercitys);
  //   const filcitys = this.mastercitys.items.filter(
  //     city => city.admin_area_id == idprovinsi
  //   );
  //   this.mastercity = filcitys;
  //   console.log('city', this.mastercity);
  // }
  // filterdistrics(event: { component: IonicSelectableComponent; value: any }) {
  //   const pilihkabupaten = event.value;
  //   console.log('kabupaten', pilihkabupaten);
  //   const idkabupaten = pilihkabupaten.admin_area_id;
  //   this.domisiliidkab = pilihkabupaten.admin_area_id;
  //   this.domisilinamakab = pilihkabupaten.city_name;
  //   console.log('kabupaten', this.domisiliidkab, this.domisilinamakab);
  //   // master kabupaten yang sudah di filter
  //   console.log('service distric', this.masterdistrics);
  //   const fildistrics = this.masterdistrics.items.filter(
  //     distric => distric.city_id == idkabupaten
  //   );
  //   this.masterdistric = fildistrics;
  //   console.log('kecamataan', this.masterdistric);
  // }

  filterkodeposs(event: { component: IonicSelectableComponent; value: any }) {
    const pilihkecamatan = event.value;
    console.log('kecamatan', pilihkecamatan);
    const idkecamatan = pilihkecamatan.id_kecamatan;
    this.domisiliidkec = pilihkecamatan.id_kecamatan;
    this.domisilinamakec = pilihkecamatan.nama;
    this.domidkec = this.domisiliidkec;
    this.domnamakec = this.domisilinamakec;
    console.log('kabupaten', this.domisiliidkec, this.domisilinamakec);
    console.log('id kecamatan terpiliha', idkecamatan);
    // master kecamatan yang sudah di filter
    console.log('service kodepos', this.masterkodeposs);
    const filkodeposs = this.masterkodeposs.filter(
      kodepos => kodepos.id_kecamatan == idkecamatan
    );
    this.masterkodepos = filkodeposs;
    console.log('kodepos', this.masterkodepos);

  }
  kodepos(event: { component: IonicSelectableComponent; value: any }) {
    const pilihkodepos = event.value;
    this.domisiliidkel = pilihkodepos.id_kode_pos;
    this.domisilinamakel = pilihkodepos.id_kode_pos;
    this.domidkel = this.domisiliidkel;
    this.domnamakel = this.domisilinamakel;
    // console.log('idkodepos', this.domisiliidkel, this.domisilinamakel);
  }

  hideList() {
    // console.log('hide');
    this.hideMe = false;
    this.domalamat = this.gss.getalamat;
    this.domidkel = this.gss.getkelid;
    this.domidkec = this.gss.getkecid;
    this.domidkab = this.gss.getkabid;
    this.domidprop = this.gss.getpropid;
    this.domnamakel = this.gss.getkelnama;
    this.domnamakec = this.gss.getkecnama;
    this.domnamakab = this.gss.getkabnama;
    this.domnamaprop = this.gss.getpropnama;

    // this.gss.indomisilialamat = this.domalamat;
    // this.gss.indomisilikel = this.domidkel;
    // this.gss.indomisilikec = this.domidkec;
    // this.gss.indomisilikab = this.domidkab;
    // this.gss.indomisiliprop = this.domidprop;
    // this.gss.indomisilinamakel = this.domnamakel;
    // this.gss.indomisilinamakec = this.domnamakec;
    // this.gss.indomisilinamakab = this.domnamakab;
    // this.gss.indomisilinamaprop = this.domnamaprop;

    console.log('hide', this);
}
  showList() {
    // console.log('show');
    this.hideMe = true;
    console.log('show', this.domalamat);
  }
  submit() {
    if (this.hideMe === true){
      this.domalamat = this.domisilialamat;
    } else {
      this.domalamat = this.gss.getalamat;
    }

    this.gss.innohp = this.NOHP;
    this.gss.injeniskelamin = this.jeniskelamin;
    this.gss.inpekerjaan = this.idpekerjaan;
    this.gss.innamapekerjaan = this.namapekerjaan;
    this.gss.inemail = this.email;
    this.gss.innokerabat = this.nokerabat;
    this.gss.innamakerabat = this.namakerabat;
    this.gss.indomisilialamat = this.domalamat;

    this.gss.indomisilikel = this.domidkel;
    this.gss.indomisilikec = this.domidkec;
    this.gss.indomisilikab = this.domidkab;
    this.gss.indomisiliprop = this.domidprop;
    this.gss.indomisilinamakel = this.domnamakel;
    this.gss.indomisilinamakec = this.domnamakec;
    this.gss.indomisilinamakab = this.domnamakab;
    this.gss.indomisilinamaprop = this.domnamaprop;
    this.router.navigate(['/p7datakeluarga']);
    console.log('data diri', this.gss);
  }
  portChange(event: { component: IonicSelectableComponent; value: any }) {
    // console.log('port:', event.value);
  }
}
