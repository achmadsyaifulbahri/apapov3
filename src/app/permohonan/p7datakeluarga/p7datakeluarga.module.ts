import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { P7datakeluargaPage } from './p7datakeluarga.page';

const routes: Routes = [
  {
    path: '',
    component: P7datakeluargaPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [P7datakeluargaPage]
})
export class P7datakeluargaPageModule {}
