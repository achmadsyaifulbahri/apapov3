import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { P7datakeluargaPage } from './p7datakeluarga.page';

describe('P7datakeluargaPage', () => {
  let component: P7datakeluargaPage;
  let fixture: ComponentFixture<P7datakeluargaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ P7datakeluargaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(P7datakeluargaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
