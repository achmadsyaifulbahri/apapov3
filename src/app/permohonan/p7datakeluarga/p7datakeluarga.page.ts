import { DatamasterService } from './../../services/datamaster.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DukcapilService } from './../../services/dukcapil.service';
import { IonicSelectableComponent } from 'ionic-selectable';

@Component({
  selector: 'app-p7datakeluarga',
  templateUrl: './p7datakeluarga.page.html',
  styleUrls: ['./p7datakeluarga.page.scss'],
})
export class P7datakeluargaPage implements OnInit {
  public namaibu = '';
  public nikibu = '';
  public negibu : any;
  public tmptibu : any;
  public ibutmptlahir = '';
  public tglibu = '';
  public alamatibu = '';

  public namaayah = '';
  public nikayah = '';
  public negayah : any;
  public tmptayah : any;
  public tglayah = '';
  public alamatayah = '';
  public masterkabupatens = '';
  public masternegaras = '';

  public ibuKodeNegara = '';
  public ibuNamaNegara = '';
  public ibuIdUuid = '';
  public ibuTempatName = ''

  constructor(private gss: DukcapilService, private router: Router, private dm: DatamasterService) { }

  ngOnInit() {
    this.dm.getLocalDataKabupaten().subscribe(kabupatens => {
      this.masterkabupatens = this.dm.dkabupaten;
      // console.log('service', this.masterkabupatens);
    });
    this.dm.getLocalDataNegara().subscribe(negaras => {
      this.masternegaras = this.dm.dnegara;
      // console.log('service', this.masternegaras);
    });
  }

  submit() {
    this.gss.innikibu = this.nikibu;
    this.gss.inidnegibu = this.negibu.kode_negara;
    this.gss.innamanegibu = this.negibu.negara;
    this.gss.inidtmptibu = this.tmptibu.id_uuid;
    this.gss.innamatmptibu = this.tmptibu.name;
    this.gss.intglibu = this.tglibu;
    this.gss.inalamatibu = this.alamatibu;
    this.gss.innamaayah = this.namaayah;
    this.gss.innikayah = this.nikayah;
    this.gss.inidnegayah = this.negayah.kode_negara;
    this.gss.innamanegayah = this.negayah.negara;
    this.gss.inidtmptayah = this.tmptayah.id_uuid;
    this.gss.innamatmptayah = this.tmptayah.name;
    this.gss.intglayah = this.tglayah;
    this.gss.inalamatayah = this.alamatayah;
    this.router.navigate(['/p8datapasangan']);
    console.log('data keluarga', this);
  }
  kabupatens(event: { component: IonicSelectableComponent; value: any }) {
    console.log('port:', event.value);
  }
  negaras(event: { component: IonicSelectableComponent; value: any }) {
    console.log('port:', event.value);
  }
}
