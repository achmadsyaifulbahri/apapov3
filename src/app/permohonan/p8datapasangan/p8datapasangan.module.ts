import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { P8datapasanganPage } from './p8datapasangan.page';

const routes: Routes = [
  {
    path: '',
    component: P8datapasanganPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [P8datapasanganPage]
})
export class P8datapasanganPageModule {}
