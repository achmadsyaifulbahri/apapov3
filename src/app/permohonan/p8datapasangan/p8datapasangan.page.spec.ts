import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { P8datapasanganPage } from './p8datapasangan.page';

describe('P8datapasanganPage', () => {
  let component: P8datapasanganPage;
  let fixture: ComponentFixture<P8datapasanganPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ P8datapasanganPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(P8datapasanganPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
