import { DatamasterService } from 'src/app/services/datamaster.service';
import { Component, OnInit } from '@angular/core';
import { DukcapilService } from 'src/app/services/dukcapil.service';
import { Router } from '@angular/router';
import { IonicSelectableComponent } from 'ionic-selectable';

@Component({
	selector: 'app-p8datapasangan',
	templateUrl: './p8datapasangan.page.html',
	styleUrls: [ './p8datapasangan.page.scss' ]
})
export class P8datapasanganPage implements OnInit {
	public namapsg = '';
	public nikpsg = '';
	public negpsg : any;
	public tmptpsg : any;
	public tglpsg : any ;
	public alamatpsg = '';
	public masterkabupatens = '';
	public masternegaras = '';
	constructor(private gss: DukcapilService, private router: Router, private dm: DatamasterService) {}

	ngOnInit() {
		this.dm.getLocalDataKabupaten().subscribe((kabupatens) => {
			this.masterkabupatens = this.dm.dkabupaten;
			// console.log('service', this.masterkabupatens);
		});
		this.dm.getLocalDataNegara().subscribe((negaras) => {
			this.masternegaras = this.dm.dnegara;
			// console.log('service', this.masternegaras);
		});
	}

	submit() {
		this.gss.innamapsg = this.namapsg;
		this.gss.innikpsg = this.nikpsg;
		this.gss.inidnegpsg = this.negpsg.kode_negara;
		this.gss.innamanegpsg = this.negpsg.negara;
		this.gss.inidtmptpsg = this.tmptpsg.id_uuid;
		this.gss.innamatmptpsg = this.tmptpsg.name;
		this.gss.intglpsg = this.tglpsg;
		this.gss.inalamatpsg = this.alamatpsg;
		this.router.navigate([ '/p13dataverifikasi' ]);
		console.log('data pasangan', this);
	}
	kabupatens(event: { component: IonicSelectableComponent; value: any }) {
		console.log('port:', event.value);
	}
	negaras(event: { component: IonicSelectableComponent; value: any }) {
		console.log('port:', event.value);
	}
}
