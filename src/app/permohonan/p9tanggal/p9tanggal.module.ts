import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { P9tanggalPage } from './p9tanggal.page';

const routes: Routes = [
  {
    path: '',
    component: P9tanggalPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [P9tanggalPage]
})
export class P9tanggalPageModule {}
