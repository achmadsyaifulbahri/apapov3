import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { P9tanggalPage } from './p9tanggal.page';

describe('P9tanggalPage', () => {
  let component: P9tanggalPage;
  let fixture: ComponentFixture<P9tanggalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ P9tanggalPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(P9tanggalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
