import { DukcapilService } from '../../services/dukcapil.service';
import { DatamasterService } from 'src/app/services/datamaster.service';
import { Component, ChangeDetectionStrategy, ViewChild, TemplateRef, ViewEncapsulation, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import {
	CalendarEvent,
	CalendarMonthViewBeforeRenderEvent,
	CalendarView,
	CalendarMonthViewDay,
	CalendarDateFormatter
} from 'angular-calendar';
import { DayViewHour } from 'calendar-utils';
import { AlertController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { CustomDateFormatter } from 'src/app/formatter/custom-date-formatter.provider';
// const moment = require('moment');
import { isSameMonth, isSameDay } from 'date-fns';
import * as moment from 'moment';
import { formatDate } from '@angular/common';

@Component({
	selector: 'app-p9tanggal',
	templateUrl: './p9tanggal.page.html',
	changeDetection: ChangeDetectionStrategy.OnPush,
	encapsulation: ViewEncapsulation.None,
	styles: [
		`
      .belumbuka {
        background-color: yellow !important;
      }

      .agus {
        background-color : white !important;
      }

      .tersedia {
        background-color: green !important;
      }
      .tersedia:hover {
        background-color: blue !important;
      }

      .habis {
        background-color: red !important;
      }

      .libur {
        background-color: gray !important;
      }
      .terpilih {
        background-color: blue !important;
      }
    `
	],
	providers: [
		{
			provide: CalendarDateFormatter,
			useClass: CustomDateFormatter
		}
	]
})
export class P9tanggalPage implements OnInit {
	// LOCAL STATES
	selectedMonthViewDay: CalendarMonthViewDay;
	selectedDayViewDate: Date;
	dayView: DayViewHour[];
	selectedDays: any = [];
	view: CalendarView = CalendarView.Month;
	CalendarView = CalendarView;

	viewDate: Date = new Date();
	modalData: {
		action: string;
		event: CalendarEvent;
	};
	activeDayIsOpen = true;
	refresh: Subject<any> = new Subject();
	detailkanim;
	masterquotaavail: any;
	dataCalendar: any;
	// constructor CyclE
	constructor(
		private alertController: AlertController,
		private router: Router,
		private activatedRoute: ActivatedRoute,
		private dm: DatamasterService,
		private gss: DukcapilService
	) {
		this.activatedRoute.queryParams.subscribe((params) => {
			// if (params && params.data) {
			// console.log('ini d page baru coy d p9tanggal', JSON.parse(params.data))
			this.dataCalendar = JSON.parse(params.data);
			// }
		});
	}

	// ngOnInit Cycle
	ngOnInit() {}

	setView(view: CalendarView) {
		this.view = view;
	}

	closeOpenMonthViewDay() {
		this.activeDayIsOpen = false;
	}
	// Melakukan Proses Pre-Munculin Month View
	beforeMonthViewRender(renderEvent: CalendarMonthViewBeforeRenderEvent): void {
		// console.log(renderEvent);
		renderEvent.body.forEach((day: any) => {
			let selectedDay = moment(day.date).format('YYYY-MM-DD');
			// console.log(selectedDay)
			let dataPembanding = this.dataCalendar.find((x: any) => {
				return moment(x.DATE).format('YYYY-MM-DD') === selectedDay;
			});
			// console.log('data Pembanding', dataPembanding)
			if (
				moment(day.date).format('ddd') === 'Sun' ||
				moment(day.date).format('ddd') === 'Sat' ||
				moment(moment(day.date).format('YYYY-MM-DD')).isBefore(moment(moment().format('YYYY-MM-DD')))
			) {
				day.cssClass = 'agus';
			} else {
				if (dataPembanding !== undefined) {
					if (dataPembanding.AVAIL_TOTAL !== 0 && dataPembanding.QUOTA_TOTAL !== 0) {
						// green
						day.cssClass = 'tersedia';
					}
					if (dataPembanding.AVAIL_TOTAL == 0 && dataPembanding.QUOTA_TOTAL !== 0) {
						// RED
						day.cssClass = 'habis';
					}
				} else {
					// kuning
					day.cssClass = 'belumbuka';
				}
			}
		});
	}
	async alerthabis() {
		const alert = await this.alertController.create({
			header: 'Warning',
			message: 'Kuota Belum Tersedia'
		});
		await alert.present();
	}
	async alertsedia() {
		const alert = await this.alertController.create({
			header: 'Accept',
			message: 'Kuota Tersedia<br>Pagi tersedia 25 kuota<br>Siang tersedia 15 Kuota'
		});

		await alert.present();
	}
	// async dayClicked(e) {
	//   e = new Date();
	//   console.log(e);
	// //   const selectedDay = moment(e.day.date).format('DD MMMM YYYY');
	// //   const hariIni = moment().format('DD MMMM YYYY');
	// //   const hariEsok = moment()
	// //     .add('1', 'day')
	// //     .format('DD MMMM YYYY');

	// //   if (selectedDay !== hariIni && selectedDay !== hariEsok) {
	// //     this.alerthabis();
	// //   } else {
	// //     e.day.cssClass = await 'terpilih';
	// //     console.log(selectedDay);
	// //     this.alertsedia();
	// //   }
	// }

	dayClicked({ date }: { date: Date }): void {
		console.log(formatDate(date, 'yyy-MM-dd', 'en-US'));
		if (isSameMonth(date, this.viewDate)) {
			// console.log(date)
			if (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) {
				this.activeDayIsOpen = false;
			} else {
				this.activeDayIsOpen = true;
				this.viewDate = date;
			}
		}
	}
}
