import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PermohonanPage } from './permohonan.page';

describe('PermohonanPage', () => {
  let component: PermohonanPage;
  let fixture: ComponentFixture<PermohonanPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PermohonanPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PermohonanPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
