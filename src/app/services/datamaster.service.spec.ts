import { TestBed } from '@angular/core/testing';

import { DatamasterService } from './datamaster.service';

describe('DatamasterService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DatamasterService = TestBed.get(DatamasterService);
    expect(service).toBeTruthy();
  });
});
