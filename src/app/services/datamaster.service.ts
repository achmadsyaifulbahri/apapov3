import { DataTujuan } from './../permohonan/p6datadiri/p6datadiri.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
declare function require(url : string);

@Injectable({
  providedIn: 'root'
})
export class DatamasterService {
  public dnegara: any;
  public dtujuan: any;
  public dkodepos: any;
  public dkecamatan: any;
  public dkabupaten: any;
  public dprovinsi: any;
  public dpekerjaan: any;
  public dkanim: any;
  public dadmin_area: any;
  public dcity: any;
  public ddistric: any;
  public dquotaavail;
  constructor(private http: HttpClient) {}

  async getdatakanim(
    Servicetypeid: string,
    Searchkey: string,
    Isactive: string,
    Rowlength: string,
    Rowstart: string,
    Flag: string,
    Token: string
  ) {
    return this.http
      .post(
        'http://10.0.11.23:7501/layanan/wni/business/select',
        // 'http://10.0.11.33:5001/layanan/wni/business/select',
        // 'https://10.0.11.33:5086/layanan/wni/business/select',
        // 'http://idejkt.imigrasi.go.id:8086/api/dukcapil/adminduk_ide/searchbynik',
        {
          SERVICE_TYPE_ID: Servicetypeid,
          SEARCHKEY: Searchkey,
          IS_ACTIVE: Isactive,
          ROW_LENGTH: Rowlength,
          ROW_START: Rowstart,
          FLAG: Flag,
          TOKEN: Token
        },
        {
          headers: {
            'Content-Type': 'application/json; charset=utf-8',
						// 'Accept': 'application/json',
						'Access-Control-Allow-Origin': '*'
            // 'Content-Type': 'application/json',
            // 'Access-Control-Allow-Origin': '*'
          }
        }
      )
      .toPromise()
      .then((res: any) => res);
  }

  async getAvailableQuota(
    BussinersId: string,
    Token: string
  ) {
    return this.http
      .post(
        'http://10.0.11.23:7501/layanan/wni/quota/availability',
        // 'http://10.0.11.33:5001/layanan/wni/quota/availability',
        {
          BUSINESS_UNIT_ID: BussinersId,
          TOKEN: Token
        },
        {
          headers: {
            'Content-Type': 'application/json'
          }
        }
      )
      .toPromise()
      .then((res: any) => res);
  }

 getquotaavail() {
   var hasil : any = require('../../assets/json/quotaavailability.json')
   console.log(hasil)
   return hasil
    // return this.http.get('/assets/json/quotaavailability.json').pipe(
    //   map(quotaavails => {
    //     console.log('file json quota', quotaavails);
    //     this.dquotaavail = quotaavails;
    //   })
    // );
  }

  getLocalDatakanim() {
    return this.http.get('/assets/json/kanim.json').pipe(
      map(kanims => {
        console.log('file json', kanims);
        this.dkanim = kanims;
      })
    );
  }
  
  getLocalDataTujuan() {
    return this.http.get('/assets/json/tujuan.json').pipe(
      map(tujuans => {
        // console.log('file json', tujuans);
        this.dtujuan = tujuans;
      })
    );
  }
  getLocalDataPekerjaan() {
    return this.http.get('/assets/json/pekerjaan.json').pipe(
      map(pekerjaans => {
        // console.log('file json', pekerjaans);
        this.dpekerjaan = pekerjaans;
      })
    );
  }
  getLocalDataNegara() {
    return this.http.get('/assets/json/negara.json').pipe(
      map(negaras => {
        // console.log('file json', tujuans);
        this.dnegara = negaras;
      })
    );
  }
  getLocalDataProvinsi() {
    return this.http.get('/assets/json/provinsi.json').pipe(
      map(provinsis => {
        // console.log('file json', provinsis);
        this.dprovinsi = provinsis;
      })
    );
  }
  getLocalDataadmin_area() {
    return this.http.get('/assets/json/admin_area.json').pipe(
      map(admin_areas => {
        // console.log('file json', admin_areas);
        this.dadmin_area = admin_areas;
      })
    );
  }
  getLocalDataKabupaten() {
    return this.http.get('/assets/json/kabupaten.json').pipe(
      map(kabupatens => {
        // console.log('file json', kabupatens);
        this.dkabupaten = kabupatens;
      })
    );
  }
  getLocalDatacity() {
    return this.http.get('/assets/json/city.json').pipe(
      map(citys => {
        // console.log('file json', citys);
        this.dcity = citys;
      })
    );
  }
  getLocalDataKecamatan() {
    return this.http.get('/assets/json/kecamatan.json').pipe(
      map(kecamatans => {
        // console.log('file json', kecamatans);
        this.dkecamatan = kecamatans;
      })
    );
  }
  getLocalDatadistric() {
    return this.http.get('/assets/json/distric.json').pipe(
      map(districs => {
        // console.log('file json', districs);
        this.ddistric = districs;
      })
    );
  }
  getLocalDataKodePos() {
    return this.http.get('/assets/json/kodepos.json').pipe(
      map(kodeposs => {
        // console.log('file json', kodeposs);
        this.dkodepos = kodeposs;
      })
    );
  }
}
