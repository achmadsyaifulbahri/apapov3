import { TestBed } from '@angular/core/testing';

import { DukcapilService } from './dukcapil.service';

describe('DukcapilService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DukcapilService = TestBed.get(DukcapilService);
    expect(service).toBeTruthy();
  });
});
