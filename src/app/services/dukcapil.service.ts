import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DukcapilService {
  public intempnik = '';
public token = ''
  public inidtujuan = '';
  public innamatujuan = '';

  public inidlayanan = '';
  public innamalayanan = '';

  public inidkanim = '';
  public innamakanim = '';

  public inidjenis = '';
  public innamajenis = '';
  public inhargajenis = '';

  public getnik = '';
  public getnokk = '';
  public getnamalgkp = '';
  public getnamalgkpibu = '';
  public gettmptlhr = '';
  public gettgllhr = '';
  public getagama = '';
  public getstatus = '';
  public getpddk = '';
  public getkerja = '';
  public getalamat = '';
  public getrt = '';
  public getrw = '';
  public getkelid = '';
  public getkecid = '';
  public getkabid = '';
  public getpropid = '';
  public getkelnama = '';
  public getkecnama = '';
  public getkabnama = '';
  public getpropnama = '';
  public innohp = '';
  public injeniskelamin = '';
  // public innamajeniskelamin = '';
  public inpekerjaan = '';
  public innamapekerjaan = '';
  public inemail = '';
  public innokerabat = '';
  public innamakerabat = '';

  public innikibu = '';
  public inidnegibu = '';
  public innamanegibu = '';
  public inidtmptibu = '';
  public innamatmptibu = '';
  public intglibu = '';
  public inalamatibu = '';

  public innamaayah = '';
  public innikayah = '';
  public inidnegayah = '';
  public innamanegayah = '';
  public inidtmptayah = '';
  public innamatmptayah = '';
  public intglayah = '';
  public inalamatayah = '';

  public innamapsg = '';
  public innikpsg = '';
  public inidnegpsg = '';
  public innamanegpsg = '';
  public inidtmptpsg = '';
  public innamatmptpsg = '';
  public intglpsg = '';
  public inalamatpsg = '';

  public indomisilialamat = '';
  public indomisilikel = '';
  public indomisilikec = '';
  public indomisilikab = '';
  public indomisiliprop = '';
  public indomisilinamakel = '';
  public indomisilinamakec = '';
  public indomisilinamakab = '';
  public indomisilinamaprop = '';


  constructor(private httpclient: HttpClient) {}

  async getdata( user_id: string, NIK: string, Password: string) {
    return this.httpclient
      .post(
        // 'http://172.16.160.128:8000/dukcapil/get_json/IMIGRASI/CALL_NIK',
        environment.urlIDE + '/api/dukcapil/adminduk_ide/searchbynik',
        {
          user_id: user_id,
          NIK: NIK,
          password: Password
        },
        {
          headers: {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': 'http://localhost:8100',
            'Access-Control-Allow-Methods': 'GET, POST, OPTIONS',
            'Access-Control-Allow-Headers': 'Content-Type'
          }
        }
      )
      .toPromise()
      .then((res: any) => res);
  }
}
