import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LoginuserService {

  constructor(private httpclient: HttpClient) { }

  async getdata(
    Username: string,
    Password: string,
  ) {
    return this.httpclient.post(
      // 'http://172.16.160.128:8000/dukcapil/get_json/IMIGRASI/CALL_NIK',
      environment.urlMOBILE + '/layanan/wni/user/login',
      {
        'USERNAME' : Username,
        'PASSWORD' : Password
        },
        {
          headers: {
            'Content-Type': 'application/json; charset=utf-8',
            'X-oracle-dms-ecid': 'outvi140000000000',
            'X-oracle-dms-rid': '0:1',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'GET, POST, OPTIONS'
          }
        }
    )
    .toPromise()
    .then((res: any) => res);
  }
}
