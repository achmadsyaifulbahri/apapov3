import { TestBed } from '@angular/core/testing';

import { RegqueueService } from './regqueue.service';

describe('RegqueueService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RegqueueService = TestBed.get(RegqueueService);
    expect(service).toBeTruthy();
  });
});
