import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DukcapilService } from './dukcapil.service';

@Injectable({
  providedIn: 'root'
})
export class RegqueueService {
  public headTOKEN = '';
  public headUNITQUOTAID = '';
  public headPRODUCTTYPEID = '';
  public headSERVICETYPEID = '';
  public headPLATFORMAND = '';
  public headPLATFORMIOS = '';
  
  public detAPPLICANTIDCARDNUMBER = '';
  public detAPPLICANTIDCARDISSUEDATE = ''; // TIDAK ADA
  public detAPPLICANTFULLNAME = '';
  public detAPPLICANTGENDER = '';
  public detAPPLICANTCIVILSTATUSID = '';
  public detAPPLICANTDATEOFBIRTH = '';
  public detAPPLICANTPLACEOFBIRTH = '';
  public detAPPLICANTPHONENUMBER = '';
  public detAPPLICANTJOBID = '';
  public detAPPLICANTEMAIL = '';
  public detAPPLICANTREACHEDPERSON = '';
  public detAPPLICANTPURPOSEID = '';
  public detIDCARDADDRESS = '';
  public detIDCARDADMINAREAID = '';
  public detIDCARDCITYID = '';
  public detIDCARDDISTRICTSID = '';
  public detIDCARDPOSTALCODEID = '';
  public detAPPLICANTADDRESS = '';
  public detAPPLICANTADMINAREAID = '';
  public detAPPLICANTCITYID = '';
  public detAPPLICANTDISTRICTSID = '';
  public detAPPLICANTPOSTALCODEID = '';
  public detMOTHERFULLNAME = '';
  public detMOTHERNATIONALITY = '';
  public detMOTHERDATEOFBIRTH = '';
  public detMOTHERPLACEOFBIRTH = '';
  // non mandatory
  public detFATHERFULLNAME = '';
  public detFATHERNATIONALITY = '';
  public detFATHERDATEOFBIRTH = '';
  public detFATHERPLACEOFBIRTH = '';
  public detPARENTSADDRESS = '';
  public detSPOUSEFULLNAME = '';
  public detSPOUSENATIONALITY = '';
  public detSPOUSEDATEOFBIRTH = '';
  public detSPOUSEPLACEOFBIRTH = '';
  public detSPOUSEADDRESS = '';

  constructor(private httpclient: HttpClient, private gss: DukcapilService) {}
  getdata() {
    this.headTOKEN = '9022EDF4424E8DB1E053A202010AC95E';
    this.headUNITQUOTAID = '';
    this.headPRODUCTTYPEID = '';
    this.headSERVICETYPEID = '';
    this.headPLATFORMAND = '';
    this.headPLATFORMIOS = '';
  
    this.detAPPLICANTIDCARDNUMBER = '';
    this.detAPPLICANTIDCARDISSUEDATE = ''; // TIDAK ADA
    this.detAPPLICANTFULLNAME = '';
    this.detAPPLICANTGENDER = '';
    this.detAPPLICANTCIVILSTATUSID = '';
    this.detAPPLICANTDATEOFBIRTH = '';
    this.detAPPLICANTPLACEOFBIRTH = '';
    this.detAPPLICANTPHONENUMBER = '';
    this.detAPPLICANTJOBID = '';
    this.detAPPLICANTEMAIL = '';
    this.detAPPLICANTREACHEDPERSON = '';
    this.detAPPLICANTPURPOSEID = '';
    this.detIDCARDADDRESS = '';
    this.detIDCARDADMINAREAID = '';
    this.detIDCARDCITYID = '';
    this.detIDCARDDISTRICTSID = '';
    this.detIDCARDPOSTALCODEID = '';
    this.detAPPLICANTADDRESS = '';
    this.detAPPLICANTADMINAREAID = '';
    this.detAPPLICANTCITYID = '';
    this.detAPPLICANTDISTRICTSID = '';
    this.detAPPLICANTPOSTALCODEID = '';
    this.detMOTHERFULLNAME = '';
    this.detMOTHERNATIONALITY = '';
    this.detMOTHERDATEOFBIRTH = '';
    this.detMOTHERPLACEOFBIRTH = '';
  // non mandatory
    this.detFATHERFULLNAME = '';
    this.detFATHERNATIONALITY = '';
    this.detFATHERDATEOFBIRTH = '';
    this.detFATHERPLACEOFBIRTH = '';
    this.detPARENTSADDRESS = '';
    this.detSPOUSEFULLNAME = '';
    this.detSPOUSENATIONALITY = '';
    this.detSPOUSEDATEOFBIRTH = '';
    this.detSPOUSEPLACEOFBIRTH = '';
    this.detSPOUSEADDRESS = '';
  }
  // async getdata(user_id: string, NIK: string, Password: string) {
  //   return this.httpclient
  //     .post(
  //       // 'http://172.16.160.128:8000/dukcapil/get_json/IMIGRASI/CALL_NIK',
  //       environment.urlIDE + '/api/dukcapil/adminduk_ide/searchbynik',
  //       {
  //         user_id: user_id,
  //         NIK: NIK,
  //         password: Password
  //       },
  //       {
  //         headers: {
  //           'Content-Type': 'application/json',
  //           'Access-Control-Allow-Origin': 'http://localhost:8100',
  //           'Access-Control-Allow-Methods': 'GET, POST, OPTIONS',
  //           'Access-Control-Allow-Headers': 'Content-Type'
  //         }
  //       }
  //     )
  //     .toPromise()
  //     .then((res: any) => res);
  // }
}
